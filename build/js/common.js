var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 42.889859, lng: 74.6158588},
		zoom: 18
	});
	var marker = new google.maps.Marker({
		position: {lat: 42.889859, lng: 74.617126},
		map: map,
		icon: "img/marker.png"
	});
}
jQuery(document).ready(function($) {
	new WOW().init();
	 $('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.slider-nav',
		infinite: false
	});
	$('.slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		draggable: false,
		arrows: false,
		infinite: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3
				}
			}
		]
	});

	$('.slider-for .item').zoom();
	

	$('.btn-spoiler').click(function(event) {
		var text = $('.btn-spoiler span').text();
		$('.content-none').slideToggle();
		if (text == 'Показать полностью') {
			$('.btn-spoiler span').text('Скрыть');
			$('.btn-spoiler img').css('transform', 'rotate(180deg)');
		} else if (text == 'Скрыть') {
			$('.btn-spoiler span').text('Показать полностью');
			$('.btn-spoiler img').css('transform', 'rotate(0)');
		}
	});

	var nav = $('.menu');
 
	$(window).scroll(function () {
		if ($(this).scrollTop() > 65) {
			nav.addClass("fixed").fadeIn();
		} else {
			nav.removeClass("fixed").fadeIn();
		}
	});

	$('.menu-mob').click(function(event) {
		$('.menu ul').slideToggle();
	});

	$('.menu ul li a').click(function(event) {
		if ($(window).width() < 768) {
			$('.menu ul').slideToggle();	
		}
	});

	$('body').on("click","a.go_to", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = ($(id).offset().top) - 50;
        // alert(top+130);
        $('body,html').animate({scrollTop: top}, 1500);
    });

	$('.form-tel').mask('(000)-00-00-00', {placeholder: "Тел. (000)-00-00-00"});

});